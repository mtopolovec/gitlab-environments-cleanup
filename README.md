[gitlab-environments-cleanup]
=========================

## Requirements
- [`python-gitlab`] - Python GitLab API client *(At least version 1.3.0)*
- [`python-dateutil`] - A robust ISO-8601 timestamp parser, among other things
- [`pytz`] - Timezone info *(Only required if you are using Python 2)*

## Usage
This script leverages the [`python-gitlab` config file][python-gitlab-config].
Just like the `gitlab` CLI tool, you can specify a non-default Gitlab server
with the `-g`/`--gitlab` option.

You can specify any number of projects with the `-p`/`--project` option:

    $ gitlab-environments-cleanup --project <project_name> --project <project_name>

Or, you can cleanup all environments visible to you:

    $ gitlab-environments-cleanup --all-projects

This script by default does not delete active environments. If you want to stop & delete active environments also, run with force option:

    $ gitlab-environments-cleanup -f --all-projects

## License

This software is released under the [MIT License](https://opensource.org/licenses/MIT).


[gitlab-environments-cleanup]: https://gitlab.com/mtopolovec/gitlab-environments-cleanup


[`python-gitlab`]: https://github.com/gpocentek/python-gitlab
[python-gitlab-config]: http://python-gitlab.readthedocs.io/en/stable/cli.html#configuration
[`pytz`]: http://pythonhosted.org/pytz/
